# MAC0110 - MiniEP7
# <Lucas Nunes Macedo> - <10387602>
using Test
function quaseigual(v1, v2)
	erro = 0.001
	igual = abs(v1 - v2)
	if igual <= erro
		return true
	else
		return false
	end
end
function check_sin(value,x)
	return quaseigual(value,sin(x))
end
function check_cos(value,x)
	return quaseigual(value,cos(x))
end
function check_tan(value,x)
	return quaseigual(value,tan(x))
end
function taylor_sin(x)
	return taylor_sin(x,1,0)	
end
function taylor_sin(x,n,m)
	if(m<10)
		return (x^n)/factorial(big(n))-taylor_sin(x,n+2,m+1)
	else
		return (x^n)/factorial(big(n))
	end
end
function taylor_cos(x)
	return taylor_cos(x,0,0)	
end
function taylor_cos(x,n,m)
	if(m<10)
		return (x^n)/factorial(big(n))-taylor_cos(x,n+2,m+1)
	else
		return (x^n)/factorial(big(n))
	end
end

function bernoulli(n)
	n*=2
	A=Vector{Rational{BigInt}}(undef, n+1)
	for m=0: n
		A[m+1]= 1 // (m+1)
		for j=m:-1:1
			A[j]= j* (A[j] -A[j+1])
		end
	end
	return abs(A[1])
end
function taylor_tan(x)
	return taylor_tan(x,1,0)	
end
function taylor_tan(x,n,m)
	if(m<10)
		return (2^(2*n))*(2^(2*n)-1)*BigFloat(bernoulli(n))*x^(2*n-1)/factorial(big(2*n))+taylor_tan(x,n+1,m+1)
	else
		return (2^(2*n))*(2^(2*n)-1)*BigFloat(bernoulli(n))*x^(2*n-1)/factorial(big(2*n))
	end
end
function test()
	@test check_sin(taylor_sin(0),0)
	@test check_sin(taylor_sin(π),π)
	@test check_sin(taylor_sin(π/2),π/2)
	@test check_cos(taylor_cos(0),0)
	@test check_cos(taylor_cos(π),π)
	@test check_cos(taylor_cos(π/2),π/2)
	@test check_tan(taylor_tan(0),0)
	@test check_tan(taylor_tan(π/4),π/4)
end